PKG=ROI.plugin.clarabel

build:
	R CMD build .

install: build
	R CMD INSTALL ${PKG}*.tar.gz
	
check: build
	R CMD check ${PKG}*.tar.gz

test:
	Rscript tests/test_clarabel.R

manual: clean
	R CMD Rd2pdf --output=Manual.pdf .

clean:
	rm -f Manual.pdf README.knit.md README.html ${PKG}_*.tar.gz
	rm -rf .Rd2pdf*
	rm -rf ${PKG}.Rcheck

check_cran: build
	R -e "rhub::check_for_cran()"

doc:
	R -e 'roxygen2::roxygenize()'
